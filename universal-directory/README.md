# Table of Contents

* api [link](api.md)
* local development [link](local-development.md)
* Groq Query samples [link](groq-query-examples.md)
* Sanity [link](sanity.md)
* Sanity Roles [link](sanity-roles.md)
* Netlify [link](netlify.md)