# API

Locations from any Healthcare Vertical offering the "Psychiatry" Service Line.

```
*[ _type=="location" && services[].serviceLine->name match "Psychiatry"]{
    'name': name,
    'services': services[]{
        serviceLine->{
            name
        }
    }
}
```

https://www.sanity.io/blog/exporting-your-structured-content-as-csv-using-jq-in-the-command-line