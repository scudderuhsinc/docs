## GMEC > Programs (all) > Curriculum 

```
*[ _type == "gmeConsortium" && _id == "4d5ece91-b4d0-420f-86b4-f7987aa017e3" ].program[]{
    'id': _id,
    name,
    anchor,
    'link': link.home,
    track,
    duration,
    block[]{
        name,
        year,
        duration,
        'location': select( _type == 'gmecBlock' => location, location->name ),
    }
}
```

## GMEC > Program (single) > Curriculum

```
*[ _type == 'gmeConsortium' && _id == '4d5ece91-b4d0-420f-86b4-f7987aa017e3'][0]{
    'id': _id,
    name,
    'link':link.home,
    'program': *[ _type == 'gmeConsortium' && _id == ^._id][0].program[]{
        anchor == 'emergency-medicine' => {
            name,
            anchor,
            track,
            duration,
            'curriculum': block[]{
                name,
                year,
                duration,
                'location': select( _type == 'gmecBlock' => location, location->name ),
            },
        }
    },
}
```

## GMEC > Faculty (all) > Program (all)

```
*[ _type == 'gmeConsortium' && _id == '4d5ece91-b4d0-420f-86b4-f7987aa017e3'][0]{
    'id': _id,
    name,
    'link':link.home,
    dio->{
        'id': _id,
        nameFirst,
        nameMiddle,
        nameLast,
        'slug': slug.current,
        'primaryRole': roleTypePrimary->name,
        'img': {
            'alt': select( profileImage.alt == null => nameFirst+' '+nameLast, profileImage.alt),
            'src': profileImage.asset->url,
            'crop': profileImage.crop,
            'hotspot': profileImage.hotspot,
        },
        'credential':credential[]->init,
        'education':education[]{location,name,type},
        'biography':biography[]{title,block[]},
        'responsibility': roleTypeGmec.responsibility[]{
            'current': select( _type == "profileResp" => {name, descShort}, _type == "reference" => *[_type == "membership" && _id == ^._ref][0]{init,name,descShort}),
        },
    },
    'programs': *[ _type == 'gmeConsortium' && _id == ^._id].program[]{
        name,
        anchor,
        track,
        duration,
        'core': coreFaculty[]->{
            'id': _id,
            nameFirst,
            nameMiddle,
            nameLast,
            'slug': slug.current,
            'primaryRole': roleTypePrimary->name,
            'img': {
                'alt': select( profileImage.alt == null => nameFirst+' '+nameLast, profileImage.alt),
                'src': profileImage.asset->url,
                'crop': profileImage.crop,
                'hotspot': profileImage.hotspot,
            },
            'credential':credential[]->init,
            'education':education[]{location,name,type},
            'biography':biography[]{title,block[]},
            'responsibility': roleTypeGmec.responsibility[]{
                'current': select( _type == "profileResp" => {name, descShort}, _type == "reference" => *[_type == "membership" && _id == ^._ref][0]{init,name,descShort}),
            }
        },
        'dir': director->{
            'id': _id,
            nameFirst,
            nameMiddle,
            nameLast,
            'slug': slug.current,
            'primaryRole': roleTypePrimary->name,
            'img': {
                'alt': select( profileImage.alt == null => nameFirst+' '+nameLast, profileImage.alt),
                'src': profileImage.asset->url,
                'crop': profileImage.crop,
                'hotspot': profileImage.hotspot,
            },
            'credential':credential[]->init,
            'education':education[]{location,name,type},
            'biography':biography[]{title,block[]},
            'responsibility': roleTypeGmec.responsibility[]{
                'current': select( _type == "profileResp" => {name, descShort}, _type == "reference" => *[_type == "membership" && _id == ^._ref][0]{init,name,descShort}),
            }
        },
        'dirAssoc': directorAssociate[]->{
            'id': _id,
            nameFirst,
            nameMiddle,
            nameLast,
            'slug': slug.current,
            'primaryRole': roleTypePrimary->name,
            'img': {
                'alt': select( profileImage.alt == null => nameFirst+' '+nameLast, profileImage.alt),
                'src': profileImage.asset->url,
                'crop': profileImage.crop,
                'hotspot': profileImage.hotspot,
            },
            'credential':credential[]->init,
            'education':education[]{location,name,type},
            'biography':biography[]{title,block[]},
            'responsibility': roleTypeGmec.responsibility[]{
                'current': select( _type == "profileResp" => {name, descShort}, _type == "reference" => *[_type == "membership" && _id == ^._ref][0]{init,name,descShort}),
            }
        }
	}
}
```

## GMEC > Faculty (all) > Program (single)

```
*[ _type == 'gmeConsortium' && _id == '4d5ece91-b4d0-420f-86b4-f7987aa017e3'][0]{
    'id': _id,
    name,
    'link':link.home,
    dio->{
        'id': _id,
        nameFirst,
        nameMiddle,
        nameLast,
        'slug': slug.current,
        'primaryRole': roleTypePrimary->name,
        'img': {
            'alt': select( profileImage.alt == null => nameFirst+' '+nameLast, profileImage.alt),
            'src': profileImage.asset->url,
            'crop': profileImage.crop,
            'hotspot': profileImage.hotspot,
        },
        'credential':credential[]->init,
        'education':education[]{location,name,type},
        'biography':biography[]{title,block[]},
        'responsibility': roleTypeGmec.responsibility[]{
            'current': select( _type == "profileResp" => {name, descShort}, _type == "reference" => *[_type == "membership" && _id == ^._ref][0]{init,name,descShort}),
        },
    },
    'program': *[ _type == 'gmeConsortium' && _id == ^._id][0].program[]{
        anchor == 'emergency-medicine' => {
            name,
            anchor,
            track,
            duration,
            'core': coreFaculty[]->{
                'id': _id,
                nameFirst,
                nameMiddle,
                nameLast,
                'slug': slug.current,
                'primaryRole': roleTypePrimary->name,
                'img': {
                    'alt': select( profileImage.alt == null => nameFirst+' '+nameLast, profileImage.alt),
                    'src': profileImage.asset->url,
                    'crop': profileImage.crop,
                    'hotspot': profileImage.hotspot,
                },
                'credential':credential[]->init,
                'education':education[]{location,name,type},
                'biography':biography[]{title,block[]},
                'responsibility': roleTypeGmec.responsibility[]{
                    'current': select( _type == "profileResp" => {name, descShort}, _type == "reference" => *[_type == "membership" && _id == ^._ref][0]{init,name,descShort}),
                }
            },
            'dir': director->{
                'id': _id,
                nameFirst,
                nameMiddle,
                nameLast,
                'slug': slug.current,
                'primaryRole': roleTypePrimary->name,
                'img': {
                    'alt': select( profileImage.alt == null => nameFirst+' '+nameLast, profileImage.alt),
                    'src': profileImage.asset->url,
                    'crop': profileImage.crop,
                    'hotspot': profileImage.hotspot,
                },
                'credential':credential[]->init,
                'education':education[]{location,name,type},
                'biography':biography[]{title,block[]},
                'responsibility': roleTypeGmec.responsibility[]{
                    'current': select( _type == "profileResp" => {name, descShort}, _type == "reference" => *[_type == "membership" && _id == ^._ref][0]{init,name,descShort}),
                }
            },
            'dirAssoc': directorAssoc[]->{
                'id': _id,
                nameFirst,
                nameMiddle,
                nameLast,
                'slug': slug.current,
                'primaryRole': roleTypePrimary->name,
                'img': {
                    'alt': select( profileImage.alt == null => nameFirst+' '+nameLast, profileImage.alt),
                    'src': profileImage.asset->url,
                    'crop': profileImage.crop,
                    'hotspot': profileImage.hotspot,
                },
                'credential':credential[]->init,
                'education':education[]{location,name,type},
                'biography':biography[]{title,block[]},
                'responsibility': roleTypeGmec.responsibility[]{
                    'current': select( _type == "profileResp" => {name, descShort}, _type == "reference" => *[_type == "membership" && _id == ^._ref][0]{init,name,descShort}),
                }
            }
		}
	}
}
```

## Providers > GMEC > Faculty (single)

```
*[_type == 'provider' && slug.current == 'clarissa-gosney' && roleTypeGmec.activeGmec == true && roleTypeGmec.type == "faculty"][0] {
    'id': _id,
    nameFirst,
    nameMiddle,
    nameLast,
    'slug': slug.current,
    'primaryRole': roleTypePrimary->name,
    'img': {
        'alt': select( profileImage.alt == null => nameFirst+' '+nameLast, profileImage.alt),
        'src': profileImage.asset->url,
        'crop': profileImage.crop,
        'hotspot': profileImage.hotspot,
    },
    'credential':credential[]->init,
    'education':education[]{location,name,type},
    'biography':biography[]{title,block[]},
    'responsibility': roleTypeGmec.responsibility[]{
        'current': select( _type == "profileResp" => {name, descShort}, _type == "reference" => *[_type == "membership" && _id == ^._ref][0]{init,name,descShort}),
    },
    'gmecRole': *[ _type == "gmeConsortium" && _id == ^.roleTypeGmec.gmec._ref ][0]{
        name,
        'isDio': select( dio._ref == ^._id => true, false),
        'program': program[]{
            'name': name,
            'isDir': select( director._ref == ^._id => true, false),
            'isADir': select( directorAssoc._ref == ^._id => true, false),
            'isCore': coreFaculty[]._ref match ^._id,
        },
    },
}
```

## Providers > GMEC > Trainee (single)

Internal Medicine or Family Medicine programs, DO NOT show if education[].type == 'Medical School' returns `filtered: true`

Emergancy Medicine _key: `b5b957aae732`
Internal Medicine _key: `b3b1e37afd55`
Family Medicine _key: `cb2ccbd5ff97`
Transitional Year _key: `d0bf97d8772d`
Cardiology _key: `` Resedenency and Fellowship Tracks

```
*[_type == 'provider' && roleTypeGmec.activeGmec == true && roleTypeGmec.type == 'trainee' && roleTypeGmec.gmec._ref == '4d5ece91-b4d0-420f-86b4-f7987aa017e3'][0...10]{
    'id': _id,
    nameFirst,
    nameMiddle,
    nameLast,
    'slug': slug.current,
    'primaryRole': roleTypePrimary->name,
    'img': {
        'alt': select( profileImage.alt == null => nameFirst+' '+nameLast, profileImage.alt),
        'src': profileImage.asset->url,
        'crop': profileImage.crop,
        'hotspot': profileImage.hotspot,
    },
    'credential':credential[]->init,
    'education': select(
        roleTypeGmec.program == 'Internal Medicine' => education[]{ 'filtered': select(type != 'Medical School' => {location,name,type}, true)},
        roleTypeGmec.program == 'Family Medicine'   => education[]{ 'filtered': select(type != 'Medical School' => {location,name,type}, true)},
        education[]{type,name,location}
    ),
    'biography':biography[]{title,block[]},
    'gmecId': roleTypeGmec.gmec._ref,
    'gmecProgram': roleTypeGmec.program,
    'gmecTrack': roleTypeGmec.track,
    'gmecYear': roleTypeGmec.year,
}
```

## GMEC > Progams input menu

```
*[_type == 'gmeConsortium' && _id == '4d5ece91-b4d0-420f-86b4-f7987aa017e3'][0]{
  name,
  'programs': [0]{
    '_key': null,
    'name': 'select',
  }[1..-1]{
    program[]{
      _key,
      name
	}
}
}
```

## NNHS, all locations GROC query

```
*[_type == "locationGrp" && _id == "28ddf2b8-6c83-4e1d-9607-f848228d99a1"][0]{
  'name': name,
  'link': link.home,
  'locations': {
    'key': keystone->{...,},
    'ind': location[]->{...,},
    '--': alignGrp[]->{
      'name': name,
      'link': link.home,
      'locations': location[]->
    },
    '=': parallelGrp[]->{
      'name': name,
      'link': link.home,
      'locations': location[]->
    },
    'gme': gmec->{...,}
  }
}
```

```
*[_type == "locationGrp" && _id == "28ddf2b8-6c83-4e1d-9607-f848228d99a1"][0]{
  'name': name,
  'locations': {
    'ind': location[]->{...,},
    'add': *[_type == "locationGrp" && _id == "de81a190-25bf-4779-a1d6-3050917e6fc4"][0]{
      'name': name,
      'locations': location[]->
    }
  }
}
```

## HLN, all TeleHealth GROC query

```
*[_type == "vertical" && _id == "11e54bfd-93cc-4dd4-8d6a-b313f36d686a"][0]{
	'name': name,
  'hln': {
    'slug': hln.slug.current,
    'disclaimerHdr': hln.disclaimerHeader,
    'disclaimerBody': hln.disclaimerBody,
    'serviceareas': hln.serviceArea
  }
}
```

## Other Notes

```
$fields = array (
    "'name': name", "'locations': { 'ind': location[]->{...}, '--': alignGrp[]->{'name': name, 'locations':location[]->}, '=': parallelGrp[]->{'name':name, 'locations':location[]->} }", 
);
$searches = array("_id == \"28ddf2b8-6c83-4e1d-9607-f848228d99a1\"");
$sanity_query->fetch('locationGrp', $fields, $searches, 0, 1);
$locations = $sanity_query->get_result();
$context['sanity_locations'] = $locations;
```