# Sanity : Custom Roles

==== process overview  ====
1. gain access to the Sanity API
2. create your custom role
3. define permission resources for the custom role
5. add custom role to a user
===========================

1. Access Sanity API

    To access Sanity API via Command Line you will need to either create a access token -OR- if you are an administrative user determine your current access token (best)

    **option 1, create an access token**

    I) Sanity > Project Dashboard > API > Tokens > Create ‘API Token’ = “curl” with Permissions = “Administrator”

    **option 2, use your current token (best)**

    I) If your account has 'Administrator' privileges
    II) from a terminal window, inside the projects 'studio' directory.

    ```
    $ cd your-code/the-project/studio
    ```

    III) get the project id and your current token

    ```
    sanity debug --secrets
    ```

    returns >>

    ```
    User:
    ID: '-- deleted --'
    Name: 'Scudder Stevens'
    Email: 'Scudder.Stevens@uhsinc.com'

    Project:
    ID: '#########' // here is the project id
    Display name: 'core'
    Studio URL: 'https://uhs-blog-builder.sanity.studio/'
    User role: 'administrator'

    Authentication:
    User type: 'normal'
    Auth token: '#########' // here is your token

    ..additional details
    ```

2. Create Custom Role

The process for creating a custom role **CAN** be done within the Sanity Project Dashboard (BEST PRACTICE) [docs](https://www.sanity.io/docs/roles#c3a0e5c181e9)

  a. From the Sanity Project Dashboard ..

  Sanity > "Core" Project Dashboard > Access > Roles > [+ create new role] button

Using the Sanity API (legacy) [docs](https://www.sanity.io/docs/roles-reference#6dce2cdf45a2)

  a. add the Custom Role via Sanity API, using `curl`

  **Explanation**

  ```
  curl -X POST -H "Authorization: ${apiBearerToken}" -H "Content-Type: application/json" -d '{"title": "< menu item name >", "name": "< system id >", "description": "< who would need this particular costum role >"}' https://api.sanity.io/v2021-06-07/projects/89obqmi5/roles
  ```

  b. using your 'Admin' Token from step 1

  **Specific Example**

  ```
  curl -X POST -H "Authorization: Bearer skXZmTlyF9HHx1jaTRTKwbCuqwWrb8wjP5kXkNkB1xTTixA4wfeqMEb7AA9l9BXlZzGVVYnyNTBkX887U" -H "Content-Type: application/json" -d '{"title": "GMEC, Team Member", "name": "gmec", "description": "Custom GMEC role, a member of the general medical education consortium team."}' https://api.sanity.io/v2021-06-07/projects/89obqmi5/roles
  ```

  returns confirmation >>

  ```
  {"name":"gmec","title":"GMEC Team","description":"Custom GMEC role, a member of the graduate medical education consortium team.","isCustom":true,"projectId":"89obqmi5","appliesToUsers":true,"appliesToRobots":true,"grants":{}}%   
  ```

3. Define Role Permission Resource

This process **CAN** be done within the Sanity Project Dashboard (BEST PRACTICE) [docs](https://www.sanity.io/docs/roles#e156da551901)

  a. From the Sanity Project Dashboard ..

  Sanity > "Core" Project Dashboard > Access > Resources > [+ create new content resource] button

Using the Sanity API (legacy) define granular permissions for a custom role by creating a custom permission resource. [docs](https://www.sanity.io/docs/roles-reference#b794f620247e)

  PLS NOTE :: If a Resource is defined via the Sanity API, that resource can only be managed into the future using the API. So the resource **WILL NOT** be visible under the Sanity > Project Dashboard > Access > Resources

  a. define resources (documents, as defined in your project's schema) and make them availible to the Custom Role by Resource ID.

  **Explanation**

  ```
  curl \
  -X POST
  -H "Authorization: Bearer ${apiBearerToken}"
  -H "Content-Type: application/json"
  -d '{"permissionResourceType": "sanity.document.filter", "title": "Awesome Documents!", "description": "Our awesome documents", "config": {"filter": "_type == \"awesome\""}}'
  https://api.sanity.io/v2021-06-07/projects/${projectId}/permissionResources
  ```

  b. FOR EACH document, pass an object containing its resource details to the /permissionResources endpoint, using `curl`

  **Specific Example, Provider doc**

  ```
  curl -X POST -H "Authorization: Bearer skXZmTlyF9HHx1jaTRTKwbCuqwWrb8wjP5kXkNkB1xTTixA4wfeqMEb7AA9l9BXlZzGVVYnyNTBkX887U" -H "Content-Type: application/json" -d '{"permissionResourceType": "sanity.document.filter", "title": "Provider Documents", "description": "Healthcare Provider documents", "config": {"filter": "_type == \"provider\""}}' https://api.sanity.io/v2021-06-07/projects/89obqmi5/permissionResources
  ```
  
  returns confirmation and ID >>

  ```
  {"id":"res-PT3Ba2ab","name":null,"title":"Provider Documents","description":"Healthcare Provider documents","config":{"filter":"_type == \"provider\""},"isCustom":true,"permissionResourceType":"sanity.document.filter"}%
  ```

  DELETE

  ```
  ??
  curl --request DELETE 'https://api.sanity.io/v2021-06-07/projects/89obqmi5/permissionResources/res-PT3Ba2ab' --header 'Authorization: Bearer skPLLkqDgvQ8n6jnyWkH2XPY2RJzLzLh8t4lwMJfwH6oPeSvJEPsVVT6DzO2wJVOfGGOTab5VhqpfMZYv' --header 'Content-Type: application/json'
  ??
  ```

  **Specific Example, GMEC doc**

  ```
  curl -X POST -H "Authorization: Bearer skXZmTlyF9HHx1jaTRTKwbCuqwWrb8wjP5kXkNkB1xTTixA4wfeqMEb7AA9l9BXlZzGVVYnyNTBkX887U" -H "Content-Type: application/json" -d '{"permissionResourceType": "sanity.document.filter", "title": "GMEC Documents", "description": "graduate medical education consortium documents", "config": {"filter": "_type == \"gmeConsortium\""}}' https://api.sanity.io/v2021-06-07/projects/89obqmi5/permissionResources
  ```

  returns confirmation and ID >>

  ```
  {"id":"res-9jOyj8Mf","name":null,"title":"GMEC Documents","description":"graduate medical education consortium documents","config":{"filter":"_type == \"gmeConsortium\""},"isCustom":true,"permissionResourceType":"sanity.document.filter"}%  
  ```

  DELETE

  ```
  ??
  curl --request DELETE 'https://api.sanity.io/v2021-06-07/projects/89obqmi5/permissionResources/res-9jOyj8Mf' --header 'Authorization: Bearer skPLLkqDgvQ8n6jnyWkH2XPY2RJzLzLh8t4lwMJfwH6oPeSvJEPsVVT6DzO2wJVOfGGOTab5VhqpfMZYv' --header 'Content-Type: application/json'
  ??
  ```

  c. FOR EACH resource (doc) grant permissions FOR EACH permission: read, update, ...

  **Explanation**

  ```
  curl \
  -X POST
  -H "Authorization: Bearer ${apiBearerToken}"
  -H "Content-Type: application/json"
  -d '{"roleName": "< custom-role >", "permissionName": "read", "permissionResourceId": "${idReturned}"}'
  https://api.sanity.io/v2021-06-07/projects/${projectId}/grants
  ```

  **Specific Examples**

  grant 'read' permissions for custom-role (gmec) of resource, by id (res-PT3Ba2ab)

  ```
  curl -X POST -H "Authorization: Bearer skXZmTlyF9HHx1jaTRTKwbCuqwWrb8wjP5kXkNkB1xTTixA4wfeqMEb7AA9l9BXlZzGVVYnyNTBkX887U" -H "Content-Type: application/json" -d '{"roleName": "gmec", "permissionName": "read", "permissionResourceId": "res-PT3Ba2ab"}' https://api.sanity.io/v2021-06-07/projects/89obqmi5/grants
  ```
  returns confirmation >>

  ```
  {"roleName":"gmec","permissionName":"read","permissionResourceType":"sanity.document.filter","permissionResourceId":"res-PT3Ba2ab","params":{}}
  ```

  grant 'update' permissions for custom-role (gmec) of resource, by id (res-PT3Ba2ab)

  ```
  curl -X POST -H "Authorization: Bearer skXZmTlyF9HHx1jaTRTKwbCuqwWrb8wjP5kXkNkB1xTTixA4wfeqMEb7AA9l9BXlZzGVVYnyNTBkX887U" -H "Content-Type: application/json" -d '{"roleName": "gmec", "permissionName": "update", "permissionResourceId": "res-PT3Ba2ab"}' https://api.sanity.io/v2021-06-07/projects/89obqmi5/grants
  ```

  returns confirmation >>

  ```
  {"roleName":"gmec","permissionName":"update","permissionResourceType":"sanity.document.filter","permissionResourceId":"res-PT3Ba2ab","params":{}}% 
  ```

4. create user with the custom role

    a. From the Sanity Project Dashboard ..

    Sanity > Project Dashboard > Users > assign  = “GMEC Team” with Permissions = “GMEC, Team Member”


## Roles : Additional Notes


### Customize Default Roles (optional)

**This step CAN NOT be done within the Sanity Project Dashboard**

### Access Control

Legacy Sanity solution, ignore this option. [docs](https://www.sanity.io/docs/access-control)

### Customize Studio based on User Permissions

Studio input component visability, per Custom Role

[docs](https://www.sanity.io/docs/conditional-fields#1cd9fa233032)

Desk Structure supporting Custom Roles

[code](https://www.sanity.io/schemas/desk-structure-with-custom-roles-332e8ada)