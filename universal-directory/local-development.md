# README

This README document defines best practices and the required steps to get a local `development` version of the _Sanity Studio_ up and running.

## What is this repository for?

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up?

summery here

### Configuration

_Enviornmental Variables_

For security reasons 'project id' and 'dataset' are not defined within the `sanity.json` file. To run the Sanity Studio locally you will need to define those details using the 'dotenv' npm package and a `.env.*` file. Specifically you need to define the 'development' enviornment using a local `.env.devolopment` file.

    1. determin the Sanity PROJECT_ID - This should be provided by a Project Manager or a Sanity Organizational Administrator.

    2. determin the DATASET for your version of the Sanity Studio - This should be provided by a Project Manager or a Sanity Organizational Administrator. `directory` is the default for the Universal Directory Studio.

    3. determin the CONTEXT & CONTEXT_DOC_ID for your version of the Sanity Studio - This should be provided by a Project Manager or a Sanity Organizational Administrator. `corp` & `undefined` are the defaults.

    4. copy the `example.env.devolopment` and rename it `.env.devolopment`

    5. edit the `.env.devolopment` file defining the appriate enviornmental variables:

        * SANITY_STUDIO_API_PROJECT_ID=here - from step. 1
        * SANITY_STUDIO_API_DATASET=here - from step 2.
        * SANITY_STUDIO_CONTEXT=here - from step 3.
        * SANITY_STUDIO_CONTEXT_DOC_ID=here - from step 3.

When you start Sanity in your local enviornment you will get confirmation of the Enviornmental Variables used.

```
% sanity start
Using project ID from environment config (< PROJECT_ID >)
Using dataset from environment config (< DATASET > )
✔ Checking configuration files...
⠸ Compiling...
✔ Compiling...
Content Studio successfully compiled! Go to http://localhost:3333
```

If you are changing Enviornmental Variables you might need to rebuild the Studio, from the CLI:

    1. from with in the terminal window, hold [control] and hit the 'C' button to cancel current build (Mac)

    2. from the CLI prompt %, enter `sanity start` to rebuild the Studio

RESOURCES: dotenv package [npm](https://www.npmjs.com/package/dotenv) | Sanity [docs](https://www.sanity.io/docs/studio-environment-variables)

### Dependencies

### Database configuration

## How to run tests

## Deployment instructions

see `deployment.md` and `NETLIFY.md`

## Contribution guidelines

* Issue Tracking - the easiest way to contribute
* Repo (BitBucket)
* Code review


## Who do I talk to?

* Sanity 'Organization' or 'core' Administrator