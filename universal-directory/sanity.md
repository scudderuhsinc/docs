# Sanity

## Studio Context

Using the 'studio context' enviornmental variable, customized versions of the studio can be defined for specific use cases.

Contexts:

`corp` - all records [all](https://studio.uhs.com)

Graduate Medical Education Consortiums

* `gmec` - all gmec records [all](https://gmec.uhs.com)
* Individual consortium's records:
    * [SoCal](https://gmec.socalresidency.com)
    * [VHS](https://gmec.valleyhealthsystemlv.com)

Healthcare Verticals

* `ac`  - Acute Care [AC](https://ac.uhs.com)
* `bh`  - Behavioral Health [BH](https://bh.uhs.com)
* `ipm` - Independant Physicains Management [IPM](https://ipm.uhs.com)

## Icons

React [www](https://react-icons.github.io/react-icons/) - Remix Icons [RI](https://react-icons.github.io/react-icons/icons?name=ri)

Import the icons directly into JavaScript Documents

`import { IconName } from "react-icons/ri";`

### Datasets

https://www.sanity.io/docs/how-to-use-hot-swapping-for-datasets