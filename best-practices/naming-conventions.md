
# Naming Conventions

## Locations

### LOCATION, CHILD SERVICE LINE

Only one service line should be used within a location's name.

Location Name, the Name of Service Line

* Hamburger Heaven, Hamburgers
* Damonte Ranch, Family and Internal Medicine
* Lakewood Ranch Medical Physicians Aliance, Foot & Ankle

### PARENT LOCATION —/at CHILD LOCATION

Use at when the service line name follows the parent location name.

Parent Location Name — / at Child Location Name

* Dairy Queen — Hamburger Heaven
* NNMG - Damonte Ranch
* TexomaCare at Denison

### PARENT LOCATION (CHILD LOCATION)

Parent Location Name (Child Location as defined by a Geographic or Attraction name)

* Dairy Queen (Brooklyn) // Brooklyn, Geographic
* Universal Health Services (Executive Terrace) // Building name, Attraction
* Damonte Ranch (West) // Western part of a larger area, Geographic

### COMBINED

* NNMG - Damonte Ranch, Family and Internal Medicine
    * parent location - child location, service line
* South Texas Health System Clinics, Urology (McAllen)
    * location, service line (geo)
* TexomaCare, Ob/Gyn at Denison (Parker)
    * parent location, service line at child location (geo)