# How to write documentation

Documentation is one thing developers hate to do, but other developers hate when there is a new library or tool with no direction on how to use it. While documentation
is not a lot of fun to write, it can be super useful to your future self and others who want to use your libraries and tools. If you find yourself getting annoyed from
developers or users constantly asking you about x or y. Odds are you have a documentation
issue or at least an opportunity to create some documentation to handle future requests for the same information.

This document is a short guide for myself to try to give me something to reference as I try
to improve my documentation skills.

## Use Markdown

Markdown is a great medium for writing documentation, it allows you to leverage simple formatting
tricks that can make the user experience of consuming your documentation very pleasant and still lightweight. 

> Want to learn more about Markdown and how to use it - (https://google.com/?q=markdown%20101)

Here is a GitHub guide - https://guides.github.com/features/mastering-markdown/

## Create an README.md per feature/module

Having documentation in the source file can be great if you are trying to figure out what is going on with the function, but it can be really annoying if you are trying to maintain the function and have to scroll down all the time. I prefer to create a separate readme file per each module or feature. I found this process to be more user-friendly and creates a nice wiki-like experience in my repo on GitHub. Since GitHub by default previews each folder in your repo with the README.md file.

The one drawback I can see, is that you may not update the documentation when changing the feature or module, but my counter argument to this, is that the documentation should focus more on usage and surface area than implementation details and if you are changing your implementation details, as long as the surface area is the same your documentation should remain intact, but if you change the surface area, maybe you should consider creating a separate feature for the new surface area, but it will be a breaking change and documentation will have to change. I think practice can overcome this drawback.

## How to structure your documentation

There are several opinions on this and I think they are all good as long as your users are able to quickly understand how to use your feature or module and if the feature or module is something they want to use.

https://github.com/siren-lang/siren/blob/master/documentation/notes/organisation-and-design.md#how-to-write-documentation

You may want to check the above link out, but it seems like a good structure to follow:

- Title
- Description
- Why?
- Drawbacks
- Architecture

Then if it makes sense add the following: (Stability, Authors, License, Platforms, Portability, Category, etc)

## Formatted Commit Messages

> this is something I really want to improve on

The Angular JS team put together some solid guidelines.

https://gist.github.com/stephenparish/9941e89d80e2bc58a153

> Using the -m flag in the commit message you can just create an open single quote and write multiple lines if you are using the bash shell

```
git commit -m 'type(scope): subject

Description goes here

Action [Issue #]'
```

Example:

```
git commit -m 'feat(widgets/list): Added List for Widgets

Created list of widgets with pagination

Closed #1'
```

So this message is saying that the type is a feature and the scope is the widgets/list module/feature and the subject is Added List for Widgets, with a description and action.

Read more from the link above.

[source](https://gist.github.com/twilson63/32de4ead7e44c3ed7bafae2c81d37a02)